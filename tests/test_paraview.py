#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
import unittest
import numpy
import sys

import paraview.simple as pv
import PIL as pil

from cinema_python import cinema_store
from cinema_python import explorers
from cinema_python import pv_explorers

import compare_helper as ch


class TestBasicFileStore(unittest.TestCase):

    def clean_up(self, cs, fname):
        pass

    def test_slice(self):
        # set up some processing task
        view_proxy = pv.CreateRenderView()
        s = pv.Sphere()
        sliceFilt = pv.Slice( SliceType="Plane", Input=s, SliceOffsetValues=[0.0] )
        sliceFilt.SliceType.Normal = [0,1,0]
        sliceRep = pv.Show(sliceFilt)

        #make or open a cinema data store to put results in
        fname = "/tmp/pv_slice_data/info.json"
        cs = cinema_store.FileStore(fname)
        cs.filename_pattern = "{phi}_{theta}_{offset}_{color}_slice.png"
        cs.add_parameter("phi", cinema_store.make_parameter('phi', [90, 120, 140]))
        cs.add_parameter("theta", cinema_store.make_parameter('theta', [-90,-30,30,90]))
        cs.add_parameter("offset", cinema_store.make_parameter('offset', [-.4,-.2,0,.2,.4]))
        cs.add_parameter("color", cinema_store.make_parameter('color', ['yellow', 'cyan', "purple"], typechoice='list'))

        colorChoice = pv_explorers.ColorList()
        colorChoice.AddSolidColor('yellow', [1, 1, 0])
        colorChoice.AddSolidColor('cyan', [0, 1, 1])
        colorChoice.AddSolidColor('purple', [1, 0, 1])

        #associate control points wlth parameters of the data store
        cam = pv_explorers.Camera([0,0,0], [0,1,0], 10.0, view_proxy) #phi,theta implied
        filt = pv_explorers.Slice("offset", sliceFilt)
        col = pv_explorers.Color("color", colorChoice, sliceRep)

        params = ["phi","theta","offset","color"]
        e = pv_explorers.ImageExplorer(cs, params, [cam, filt, col], view_proxy)
        #run through all parameter combinations and put data into the store
        e.explore()

        # Reproduce an entry and compare vs. loaded

        # First set the parameters to reproduce
        cam.execute(cinema_store.Document({'theta' : -30, 'phi' : 120}))
        filt.execute(cinema_store.Document({'offset' : -.4}))
        col.execute(cinema_store.Document({'color' : 'cyan'}))
        imageslice = ch.pvRenderToArray(view_proxy)
        del view_proxy

        # Now load the corresponding entry
        cs2 = cinema_store.FileStore(fname)
        cs2.load()
        docs = []
        for doc in cs2.find({'theta' : -30, 'phi' : 120, 'offset' : -.4, 'color' : 'cyan'}):
            docs.append(doc.data)

        #print "gen entry: \n", imageslice, "\n", imageslice.shape, "\n loaded: \n", docs[0], "\n", docs[0].shape
        # compare the two
        l2error = ch.compare_l2(imageslice, docs[0])
        ncc = ch.compare_ncc(imageslice, docs[0])
        self.assertTrue((l2error < 1.0) and (ncc > 0.99))

    def test_sliceSFS(self):
        # set up some processing task
        view_proxy = pv.CreateRenderView()
        s = pv.Sphere()
        sliceFilt = pv.Slice( SliceType="Plane", Input=s, SliceOffsetValues=[0.0] )
        sliceFilt.SliceType.Normal = [0,1,0]
        sliceRep = pv.Show(sliceFilt)

        #make or open a cinema data store to put results in
        fname = "/tmp/pv_sliceSFS_data/info.json"
        cs = cinema_store.SingleFileStore(fname)
        cs.add_parameter("phi", cinema_store.make_parameter('phi', [90, 120, 140]))
        cs.add_parameter("theta", cinema_store.make_parameter('theta', [-90,-30,30,90]))
        cs.add_parameter("offset", cinema_store.make_parameter('offset', [-.4,-.2,0,.2,.4]))
        cs.add_parameter("color", cinema_store.make_parameter('color', ['yellow', 'cyan', "purple"], typechoice='list'))

        colorChoice = pv_explorers.ColorList()
        colorChoice.AddSolidColor('yellow', [1, 1, 0])
        colorChoice.AddSolidColor('cyan', [0, 1, 1])
        colorChoice.AddSolidColor('purple', [1, 0, 1])

        #associate control points wlth parameters of the data store
        cam = pv_explorers.Camera([0,0,0], [0,1,0], 10.0, view_proxy) #phi,theta implied
        filt = pv_explorers.Slice("offset", sliceFilt)
        col = pv_explorers.Color("color", colorChoice, sliceRep)

        params = ["phi","theta","offset","color"]
        e = pv_explorers.ImageExplorer(cs, params, [cam, filt, col], view_proxy)
        #run through all parameter combinations and put data into the store
        e.explore()

        # Reproduce an entry and compare vs. loaded

        # First set the parameters to reproduce
        cam.execute(cinema_store.Document({'theta' : 30, 'phi' : 140}))
        filt.execute(cinema_store.Document({'offset' : .2}))
        col.execute(cinema_store.Document({'color' : 'purple'}))
        imageslice = ch.pvRenderToArray(view_proxy)
        del view_proxy

        # Now load the corresponding
        cs2 = cinema_store.SingleFileStore(fname)
        cs2.load()
        docs = []
        for doc in cs2.find({'theta' : 30, 'phi' : 140, 'offset' : .2, 'color' : 'purple'}):
            docs.append(doc.data)

        #pil.Image.fromarray(imageslice).save("./generated.png")
        #pil.Image.fromarray(docs[0]).save("./loaded.png")
        #print "gen entry: \n", imageslice, "\n", imageslice.shape, "\n loaded: \n", docs[0], "\n", docs[0].shape
        # compare the two
        l2error = ch.compare_l2(imageslice, docs[0])
        ncc = ch.compare_ncc(imageslice, docs[0])
        success = (l2error < 1.0) and (ncc > 0.99)

        if not success:
            print "\n l2-error = ", l2error, " ; ncc = ", ncc, "\n"

        self.assertTrue(success)


    def test_contour(self):
        # set up some processing task
        view_proxy = pv.CreateRenderView()
        view_proxy.ViewSize = [1024, 768]
        s = pv.Wavelet()
        contour = pv.Contour(Input=s, ContourBy='RTData', ComputeScalars=1 )
        sliceRep = pv.Show(contour)

        #make or open a cinema data store to put results in
        fname = "/tmp/pv_contour_data/info.json"
        cs = cinema_store.FileStore(fname)
        cs.filename_pattern = "{phi}_{theta}_{contour}_{color}_contour.png"
        cs.add_parameter("phi", cinema_store.make_parameter('phi', [90,120,140]))
        cs.add_parameter("theta", cinema_store.make_parameter('theta', [-90,-30,30,90]))
        cs.add_parameter("contour", cinema_store.make_parameter('contour', [50,100,150,200]))
        cs.add_parameter("color", cinema_store.make_parameter('color', ['white', 'RTData'], typechoice='list'))

        #associate control points wlth parameters of the data store
        cam = pv_explorers.Camera([0,0,0], [0,1,0], 75.0, view_proxy) #phi,theta implied
        filt = pv_explorers.Contour("contour", contour)

        colorChoice = pv_explorers.ColorList()
        colorChoice.AddSolidColor('white', [1,1,1])
        colorChoice.AddLUT('RTData', pv.GetLookupTableForArray( "RTData", 1, RGBPoints=[43.34006881713867, 0.23, 0.299, 0.754, 160.01158714294434, 0.865, 0.865, 0.865, 276.68310546875, 0.706, 0.016, 0.15] )
    )
        col = pv_explorers.Color("color", colorChoice, sliceRep)

        params = ["phi","theta","contour","color"]
        e = pv_explorers.ImageExplorer(cs, params, [cam, filt, col], view_proxy)

        #run through all parameter combinations and put data into the store
        e.explore()

        # Reproduce an entry and compare vs. loaded

        # First set the parameters to reproduce
        cam.execute(cinema_store.Document({'theta' : 30, 'phi' : 140}))
        filt.execute(cinema_store.Document({'contour' : 100}))
        col.execute(cinema_store.Document({'color' : 'RTData'}))
        imageslice = ch.pvRenderToArray(view_proxy)
        pv.Delete(s)
        pv.Delete(contour)
        pv.Delete(view_proxy)

        # Now load the corresponding
        cs2 = cinema_store.FileStore(fname)
        cs2.load()
        docs = []
        for doc in cs2.find({'theta' : 30, 'phi' : 140, 'contour' : 100, 'color' : 'RTData'}):
            docs.append(doc.data)

        #pil.Image.fromarray(imageslice).save("./generated.png")
        #pil.Image.fromarray(docs[0]).save("./loaded.png")
        #print "gen entry: \n", imageslice, "\n", imageslice.shape, "\n loaded: \n", docs[0], "\n", docs[0].shape
        # compare the two
        l2error = ch.compare_l2(imageslice, docs[0])
        ncc = ch.compare_ncc(imageslice, docs[0])
        success = (l2error < 1.0) and (ncc > 0.99)

        if not success:
            print "\n l2-error = ", l2error, " ; ncc = ", ncc, "\n"

        self.assertTrue(success)


    def test_composite(self):
        # set up some processing task
        view_proxy = pv.CreateRenderView()
        s = pv.Wavelet()
        contour = pv.Contour(Input=s, ContourBy='RTData', ComputeScalars=1 )
        sliceRep = pv.Show(contour)

        #make or open a cinema data store to put results in
        fname = "/tmp/pv_composite_data/info.json"
        cs = cinema_store.FileStore(fname)
        cs.filename_pattern = "{phi}_{theta}_{contour}_{color}_contour.png"
        cs.add_parameter("phi", cinema_store.make_parameter('phi', [90,120,140]))
        cs.add_parameter("theta", cinema_store.make_parameter('theta', [-90,-30,30,90]))
        cs.add_parameter("contour", cinema_store.make_parameter('contour', [50,100,150,200], typechoice='option'))
        cs.add_parameter("color", cinema_store.make_parameter('color', ['white', 'RTData', 'depth'], typechoice='list'))

        #associate control points wlth parameters of the data store
        cam = pv_explorers.Camera([0,0,0], [0,1,0], 75.0, view_proxy) #phi,theta implied
        filt = pv_explorers.Contour("contour", contour)

        colorChoice = pv_explorers.ColorList()
        colorChoice.AddSolidColor('white', [1,1,1])
        colorChoice.AddLUT('RTData', pv.GetLookupTableForArray
                           ( "RTData", 1,
                             RGBPoints=[43.34006881713867, 0.23, 0.299, 0.754, 160.01158714294434, 0.865, 0.865, 0.865, 276.68310546875, 0.706, 0.016, 0.15] ))
        colorChoice.AddDepth('depth')

        col = pv_explorers.Color("color", colorChoice, sliceRep)

        params = ["phi","theta","contour","color"]
        e = pv_explorers.ImageExplorer(cs, params, [cam, filt, col], view_proxy)

        #run through all parameter combinations and put data into the store
        e.explore()

        # Reproduce an entry and compare vs. loaded

        # First set the parameters to reproduce
        cam.execute(cinema_store.Document({'theta' : 30, 'phi' : 140}))
        filt.execute(cinema_store.Document({'contour' : 100}))
        col.execute(cinema_store.Document({'color' : 'depth'}))
        imageslice = ch.pvRenderToArray(view_proxy)
        pv.Delete(s)
        pv.Delete(contour)
        pv.Delete(view_proxy)

        # Now load the corresponding
        cs2 = cinema_store.FileStore(fname)
        cs2.load()
        docs = []
        for doc in cs2.find({'theta' : 30, 'phi' : 140, 'contour' : 100, 'color' : 'depth'}):
            docs.append(doc.data)

        #pil.Image.fromarray(imageslice).save("./generated.png")
        #pil.Image.fromarray(docs[0]).save("./loaded.png")
        #print "gen entry: \n", imageslice, "\n", imageslice.shape, "\n loaded: \n", docs[0], "\n", docs[0].shape
        # compare the two
        l2error = ch.compare_l2(imageslice, docs[0])
        ncc = ch.compare_ncc(imageslice, docs[0])
        success = (l2error < 1.0) and (ncc > 0.99)

        if not success:
            print "\n l2-error = ", l2error, " ; ncc = ", ncc, "\n"

        self.assertTrue(success)


if __name__ == '__main__':
    unittest.main()

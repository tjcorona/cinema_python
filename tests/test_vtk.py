#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
import unittest

import numpy as np

from cinema_python import cinema_store
from cinema_python import explorers
from cinema_python import vtk_explorers

try:
    from cinema_python import OexrHelper as exr
    from cinema_python.OexrHelper import OexrCompression
except ImportError:
    print "Could not import OpenEXR, some tests will be disabled."

import vtk

import compare_helper as ch

import PIL as pil


class TestImageFileStore(unittest.TestCase):
    def clean_up(self, cs, fname):
        import os
        for doc in cs.find():
            os.remove(cs._get_filename(doc.descriptor))
        os.remove(fname)

    def test_basic(self):
        rw = vtk.vtkRenderWindow()
        rw.SetSize(1024, 768)
        r = vtk.vtkRenderer()
        rw.AddRenderer(r)

        s = vtk.vtkRTAnalyticSource()
        s.SetWholeExtent(-25,25,-25,25,-25,25)

        cf = vtk.vtkContourFilter()
        cf.SetInputConnection(s.GetOutputPort())
        cf.SetInputArrayToProcess(0,0,0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "RTData")
        cf.SetNumberOfContours(1)
        cf.SetValue(0, 200)
        cf.ComputeScalarsOn()
        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(cf.GetOutputPort())
        a = vtk.vtkActor()
        a.SetMapper(m)
        r.AddActor(a)

        rw.Render()
        r.ResetCamera()

        fname = "/tmp/test_vtk_basic/info.json"
        # Create a new Cinema store
        cs = cinema_store.FileStore(fname)
        cs.filename_pattern = "{phi}_{theta}_{contour}.png"

        # These are the parameters that we will have in the store
        cs.add_parameter("phi", cinema_store.make_parameter('phi', range(0,200,80)))
        cs.add_parameter("theta", cinema_store.make_parameter('theta', range(-180,200,80)))
        cs.add_parameter("contour", cinema_store.make_parameter('contour', [160, 200]))

        # These objects are responsible of change VTK parameters during exploration
        con = vtk_explorers.Contour('contour', cf, 'SetValue')
        cam = vtk_explorers.Camera([0,0,0], [0,1,0], 150.0, r.GetActiveCamera()) # phi,theta implied

        e = vtk_explorers.ImageExplorer(cs, ['contour', 'phi','theta'], [cam, con], rw)
        e.explore()

        # Now let's reproduce the first entry in the store

        # First set the camera to {'theta' : -180, 'phi' : 0}
        doc = cinema_store.Document({'theta' : -180, 'phi' : 0})
        cam.execute(doc)
        # Change the contour value
        cf.SetValue(0, 160)

        imageslice = ch.vtkRenderToArray(rw)

        # Now load the first entry from the store
        cs2 = cinema_store.FileStore(fname)
        cs2.load()

        docs = []
        for doc in cs2.find({'theta' : -180, 'phi' : 0, 'contour' : 160}):
            docs.append(doc.data)

        # compare the two
        #self.assertTrue(numpy.all(imageslice == docs[0]))
        #self.assertTrue(abs(la.norm(imageslice) - la.norm(docs[0])) < 1.0)
        l2error = ch.compare_l2(imageslice, docs[0])
        ncc = ch.compare_ncc(imageslice, docs[0])
        success = (l2error < 1.0) and (ncc > 0.99)

        if not success:
            print "\n l2error: ", l2error, " ; ncc = ", ncc, "\n"

        self.assertTrue(success)
        #self.clean_up(cs, "./contour.json")


    def test_clip(self):
        # set up some processing task
        s = vtk.vtkSphereSource()

        plane = vtk.vtkPlane()
        plane.SetOrigin(0, 0, 0)
        plane.SetNormal(-1, -1, 0)

        clip = vtk.vtkClipPolyData()
        clip.SetInputConnection(s.GetOutputPort())
        clip.SetClipFunction(plane)
        clip.GenerateClipScalarsOn()
        clip.GenerateClippedOutputOn()
        clip.SetValue(0)

        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(clip.GetOutputPort())

        rw = vtk.vtkRenderWindow()
        r = vtk.vtkRenderer()
        rw.AddRenderer(r)

        a = vtk.vtkActor()
        a.SetMapper(m)
        r.AddActor(a)

        #make or open a cinema data store to put results in
        fname = "/tmp/test_vtk_clip/info.json"
        cs = cinema_store.FileStore(fname)
        cs.filename_pattern = "{phi}_{theta}_{offset}_slice.png"
        cs.add_parameter("phi", cinema_store.make_parameter('phi', range(0, 200, 40)))
        cs.add_parameter("theta", cinema_store.make_parameter('theta', range(-180,200,40)))
        cs.add_parameter("offset", cinema_store.make_parameter('offset', [0,.2,.4,.6,.8,1.0]))

        #associate control points wlth parameters of the data store
        cam = vtk_explorers.Camera([0,0,0], [0,1,0], 3.0, r.GetActiveCamera()) #phi,theta implied
        g = vtk_explorers.Clip('offset', clip)
        e = vtk_explorers.ImageExplorer(cs, ['offset','phi', 'theta'], [cam, g], rw)

        #run through all parameter combinations and put data into the store
        rw.Render()
        e.explore()

        # Now let's reproduce an entry in the store

        # First set the camera to {'theta' : -140, 'phi' : 80}
        doc = cinema_store.Document({'theta' : -140, 'phi' : 80})
        cam.execute(doc)
        # Change the clip value and render
        clip.SetValue(.4)
        imageslice = ch.vtkRenderToArray(rw)

        # Now load the same entry from the store
        cs2 = cinema_store.FileStore(fname)
        cs2.load()
        docs = []
        for doc in cs2.find({'theta' : -140, 'phi' : 80, 'offset' : .4}):
            docs.append(doc.data)

        # compare the two
        l2error = ch.compare_l2(imageslice, docs[0])
        ncc = ch.compare_ncc(imageslice, docs[0])
        self.assertTrue((l2error < 1.0) and (ncc > 0.99))

        #self.clean_up(cs, "./info.json")


    def test_contour(self):
        # set up some processing task
        s = vtk.vtkRTAnalyticSource()
        s.SetWholeExtent(-50,50,-50,50,-50,50)
        cf = vtk.vtkContourFilter()
        cf.SetInputConnection(s.GetOutputPort())
        cf.SetInputArrayToProcess(0,0,0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "RTData")
        cf.SetNumberOfContours(1)
        cf.SetValue(0, 100)

        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(cf.GetOutputPort())

        rw = vtk.vtkRenderWindow()
        r = vtk.vtkRenderer()
        rw.AddRenderer(r)

        a = vtk.vtkActor()
        a.SetMapper(m)
        r.AddActor(a)

        rw.Render()
        r.ResetCamera()

        #make or open a cinema data store to put results in

        fname = "/tmp/test_vtk_contour/info.json"
        cs = cinema_store.FileStore(fname)
        cs.filename_pattern = "{contour}_{color}.png"
        cs.add_parameter("contour", cinema_store.make_parameter('contour', [0,25,50,75,100,125,150,175,200,225,250]))
        cs.add_parameter("color", cinema_store.make_parameter('color', ['white','red']))

        colorChoice = vtk_explorers.ColorList()
        colorChoice.AddSolidColor('white', [1,1,1])
        colorChoice.AddSolidColor('red', [1,0,0])

        #associate control points with parameters of the data store
        g = vtk_explorers.Contour('contour', cf, 'SetValue')
        c = vtk_explorers.Color('color', colorChoice, a)
        e = vtk_explorers.ImageExplorer(cs, ['contour','color'], [g,c], rw)

        #run through all parameter combinations and put data into the store
        e.explore()

        # Now let's reproduce an entry in the store

        # First set the parameters to {'contour' : 75} and {'color' : 'white'}
        g.execute(cinema_store.Document({'contour' : 75}))
        c.execute(cinema_store.Document({'color' : 'white'}))
        imageslice = ch.vtkRenderToArray(rw)

        # Now load the same entry from the store
        cs2 = cinema_store.FileStore(fname)
        cs2.load()
        docs = []
        for doc in cs2.find({'contour' : 75, 'color' : 'white'}):
            docs.append(doc.data)

        # compare the two
        l2error = ch.compare_l2(imageslice, docs[0])
        ncc = ch.compare_ncc(imageslice, docs[0])
        self.assertTrue((l2error < 1.0) and (ncc > 0.99))
        #self.clean_up(cs, "./info.json")


    def test_layers2(self):
        # set up some processing task
        s = vtk.vtkSphereSource()
        s.Update()
        bds = s.GetOutput().GetBounds()
        ef = vtk.vtkElevationFilter()
        ef.SetLowPoint(bds[0],bds[2],bds[4])
        ef.SetHighPoint(bds[1],bds[3],bds[5])
        ef.SetInputConnection(s.GetOutputPort())
        ef.Update()
        er = ef.GetOutput().GetPointData().GetArray("Elevation").GetRange()
        rw = vtk.vtkRenderWindow()
        r = vtk.vtkRenderer()
        rw.AddRenderer(r)

        cf = vtk.vtkContourFilter()
        cf.SetInputConnection(ef.GetOutputPort())
        cf.SetInputArrayToProcess(0,0,0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "Elevation")
        cf.SetNumberOfContours(1)
        cf.ComputeScalarsOn()
        cf.SetValue(0, 0.5)
        cf.Update()

        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(s.GetOutputPort()) #reset camera acts weird if 2D
        a = vtk.vtkActor()
        a.SetMapper(m)
        r.AddActor(a)
        rw.Render()
        r.ResetCamera()
        m.SetInputConnection(cf.GetOutputPort())
        rw.Render()

        plane = vtk.vtkPlane()
        plane.SetOrigin(0, 0, 0)
        plane.SetNormal(-1, -1, 0)
        clip = vtk.vtkClipPolyData()
        clip.SetInputConnection(s.GetOutputPort())
        clip.SetClipFunction(plane)
        clip.GenerateClipScalarsOn()
        clip.GenerateClippedOutputOn()
        clip.SetValue(0)
        m2 = vtk.vtkPolyDataMapper()
        m2.SetInputConnection(clip.GetOutputPort()) #reset camera acts weird if 2D
        a2 = vtk.vtkActor()
        a2.SetMapper(m2)
        r.AddActor(a2)

        fname = "/tmp/test_vtk_layers2/info.json"
        #make a cinema data store to put results in
        cs = cinema_store.FileStore(fname)
        cs.filename_pattern = "{phi}_{theta}_{object}.jpg"

        #define that visualization parameter exploration space
        cs.add_parameter("phi", cinema_store.make_parameter('phi', range(0,200,50)))
        cs.add_parameter("theta", cinema_store.make_parameter('theta', range(-180,200,50)))

        cs.add_layer("object", cinema_store.make_parameter('object', ['contour','slice']))

        isos = [float(x+1)/10.0*(er[1]-er[0])+er[0] for x in range(0,9,2)]
        cs.add_sublayer("contour", cinema_store.make_parameter('contour', isos), "object", "contour")
        cs.add_field("color", cinema_store.make_field('color', {'white':'rgb','red':'rgb','depth':'depth'}), "contour", isos)

        slices = [float(x+1)/10.0 for x in range(0,9,2)]
        cs.add_sublayer("slice", cinema_store.make_parameter('slice', slices), "object", "slice")
        cs.add_field("color2", cinema_store.make_field('color2', {'white':'rgb','red':'rgb','depth':'depth'}), "slice", slices)

        colorChoice = vtk_explorers.ColorList()
        colorChoice.AddSolidColor('white', [1,1,1])
        colorChoice.AddSolidColor('red', [1,0,0])
        colorChoice.AddDepth('depth')

        #associate parameters with pipeline control points (tracks)
        cam = vtk_explorers.Camera([0,0,0], [0,1,0], 3.0, r.GetActiveCamera())

        vcontrols = []
        vcontrol = vtk_explorers.ActorInLayer('contour', a)
        vcontrols.append(vcontrol)
        vcontrol = vtk_explorers.ActorInLayer('slice', a2)
        vcontrols.append(vcontrol)
        objecttrack = explorers.Layer('object', [v for v in vcontrols])

        layertrack1 = vtk_explorers.Contour('contour', cf, 'SetValue')
        layertrack2 = vtk_explorers.Clip('slice', clip)

        c1 = vtk_explorers.Color('color', colorChoice, a)
        c2 = vtk_explorers.Color('color2', colorChoice, a2)

        e = vtk_explorers.ImageExplorer(cs,
                                        ['phi','theta', 'object', 'slice', 'contour', 'color', 'color2'],
                                        [cam, objecttrack, layertrack1, layertrack2, c1, c2], rw)
        c1.imageExplorer = e
        c2.imageExplorer = e

        e.explore()

        # Now let's reproduce the first entry in the store

        # First set the camera to {'theta' : -180, 'phi' : 0}
        cam.execute(cinema_store.Document({'theta' : -180, 'phi' : 0}))
        #cf.SetValue(0, isos[1])
        #c1.execute(cinema_store.Document({'color' : 'red'}))
        clip.SetValue(slices[1])
        c2.execute(cinema_store.Document({'color2' : 'red'}))
        imageslice = ch.vtkRenderToArray(rw)

        # Now load the first entry from the store
        cs2 = cinema_store.FileStore(fname)
        cs2.load()

        docs = []
        for doc in cs2.find({'theta' : -180, 'phi' : 0, 'slice' : slices[1], 'color' : 'red', 'object' : 'slice'}):
            docs.append(doc.data)

        # compare the two
        l2error = ch.compare_l2(imageslice, docs[0])
        ncc = ch.compare_ncc(imageslice, docs[0])
        success = (l2error < 1.0) and (ncc > 0.99)


        pil.Image.fromarray(imageslice).save("./generated.png")
        pil.Image.fromarray(docs[0]).save("./loaded.png")
        if not success:
            print "\n l2error: ", l2error, " ; ncc = ", ncc, "\n"

        self.assertTrue(success)


    def test_vtk_layers(fname=None):
        import explorers
        import vtk_explorers
        import vtk

        # set up some processing task
        s = vtk.vtkRTAnalyticSource()
        s.SetWholeExtent(-50,50,-50,50,-50,50)

        rw = vtk.vtkRenderWindow()
        r = vtk.vtkRenderer()
        rw.AddRenderer(r)

        ac1 = vtk.vtkArrayCalculator()
        ac1.SetInputConnection(s.GetOutputPort())
        ac1.SetAttributeModeToUsePointData()
        ac1.AddCoordinateVectorVariable("coords", 0,1,2)
        ac1.SetResultArrayName("Coords")
        ac1.SetFunction("coords")

        ac2 = vtk.vtkArrayCalculator()
        ac2.SetInputConnection(ac1.GetOutputPort())
        ac2.SetAttributeModeToUsePointData()
        ac2.AddCoordinateVectorVariable("coords", 0,1,2)
        ac2.SetResultArrayName("radii")
        ac2.SetFunction("mag(coords)")

        ac3 = vtk.vtkArrayCalculator()
        ac3.SetInputConnection(ac2.GetOutputPort())
        ac3.SetAttributeModeToUsePointData()
        ac3.AddCoordinateVectorVariable("coords", 0)
        ac3.SetResultArrayName("CX")
        ac3.SetFunction("coords")
        ac4 = vtk.vtkArrayCalculator()
        ac4.SetInputConnection(ac3.GetOutputPort())
        ac4.SetAttributeModeToUsePointData()
        ac4.AddCoordinateVectorVariable("coords", 1)
        ac4.SetResultArrayName("CY")
        ac4.SetFunction("coords")
        ac5 = vtk.vtkArrayCalculator()
        ac5.SetInputConnection(ac4.GetOutputPort())
        ac5.SetAttributeModeToUsePointData()
        ac5.AddCoordinateVectorVariable("coords", 2)
        ac5.SetResultArrayName("CZ")
        ac5.SetFunction("coords")

        cf = vtk.vtkContourFilter()
        cf.SetInputConnection(ac5.GetOutputPort())
        cf.SetInputArrayToProcess(0,0,0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "radii")
        cf.SetNumberOfContours(1)
        cf.ComputeScalarsOn()
        cf.SetValue(0, 40)

        p2c = vtk.vtkPointDataToCellData()
        p2c.SetInputConnection(cf.GetOutputPort())
        p2c.PassPointDataOn()
        #p2c.Update()
        #print p2c.GetOutput()

        df = vtk.vtkDataSetWriter()
        df.SetInputConnection(p2c.GetOutputPort())
        df.SetFileName("/Users/demarle/Desktop/test.vtk")
        df.Write()

        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(p2c.GetOutputPort())
        a = vtk.vtkActor()
        a.SetMapper(m)
        r.AddActor(a)

        rw.Render()
        r.ResetCamera()

        #make a cinema data store to put results in
        if not fname:
            fname = "info.json"
        cs = FileStore(fname)

        cs.filename_pattern = "{phi}/{theta}/{contour}.png"
        contours = [15,30,55,70,85]
        #contours = [30,55]
        param = make_parameter('contour', contours)
        cs.add_layer("contour", param)
        cs.add_field("color",
                    make_field('color', {'white':'rgb',
                                        'red':'rgb',
                                        'depth':'depth',
                                        'lum':'luminance',
                                        'RTData':'value',
                                        'pX':'value',
                                        'pY':'value',
                                        'pZ':'value',
                                        'cX':'value',
                                        'cY':'value',
                                        'cZ':'value',
                                        'CX':'value',
                                        'CY':'value',
                                        'CZ':'value',
                                        }
                                ),
                    "contour", contours)
        cs.add_parameter("phi", make_parameter('phi', range(0,200,30)))
        cs.add_parameter("theta", make_parameter('theta', range(-180,200,60)))

        colorChoice = vtk_explorers.ColorList()
        colorChoice.AddSolidColor('white', [1,1,1])
        colorChoice.AddSolidColor('red', [1,0,0])
        colorChoice.AddDepth('depth')
        colorChoice.AddLuminance('lum')
        colorChoice.AddValueRender('RTData', vtk.VTK_SCALAR_MODE_USE_POINT_FIELD_DATA, 'RTData', 0, [0,100])
        colorChoice.AddValueRender('pX', vtk.VTK_SCALAR_MODE_USE_POINT_FIELD_DATA, 'Coords', 0, [-50,50])
        colorChoice.AddValueRender('pY', vtk.VTK_SCALAR_MODE_USE_POINT_FIELD_DATA, 'Coords', 1, [-50,50])
        colorChoice.AddValueRender('pZ', vtk.VTK_SCALAR_MODE_USE_POINT_FIELD_DATA, 'Coords', 2, [-50,50])
        colorChoice.AddValueRender('cX', vtk.VTK_SCALAR_MODE_USE_CELL_FIELD_DATA, 'Coords', 0, [-50,50])
        colorChoice.AddValueRender('cY', vtk.VTK_SCALAR_MODE_USE_CELL_FIELD_DATA, 'Coords', 1, [-50,50])
        colorChoice.AddValueRender('cZ', vtk.VTK_SCALAR_MODE_USE_CELL_FIELD_DATA, 'Coords', 2, [-50,50])
        colorChoice.AddValueRender('CX', vtk.VTK_SCALAR_MODE_USE_CELL_FIELD_DATA, 'CX', 0, [-50,50])
        colorChoice.AddValueRender('CY', vtk.VTK_SCALAR_MODE_USE_CELL_FIELD_DATA, 'CY', 0, [-50,50])
        colorChoice.AddValueRender('CZ', vtk.VTK_SCALAR_MODE_USE_CELL_FIELD_DATA, 'CZ', 0, [-50,50])

        #associate control points wlth parameters of the data store
        cam = vtk_explorers.Camera([0,0,0], [0,1,0], 300.0, r.GetActiveCamera())
        layertrack = vtk_explorers.Contour('contour', cf, 'SetValue')
        c = vtk_explorers.Color('color', colorChoice, a)
        e = vtk_explorers.ImageExplorer(cs, ['phi','theta','contour','color'], [cam, layertrack, c], rw)
        c.imageExplorer = e
        e.explore()

    def test_exr_export_z(self):
        try:
            exr;
        except NameError:
            print "OpenEXR test disabled: No OpenEXR available."
            return

        # generate a test image
        rw = vtk.vtkRenderWindow()
        rw.SetSize(1920, 1080) # test HD resolution to compare sizes
        r = vtk.vtkRenderer()
        rw.AddRenderer(r)

        s = vtk.vtkRTAnalyticSource()
        s.SetWholeExtent(-25,25,-25,25,-25,25)

        cf = vtk.vtkContourFilter()
        cf.SetInputConnection(s.GetOutputPort())
        cf.SetInputArrayToProcess(0,0,0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "RTData")
        cf.SetNumberOfContours(1)
        cf.SetValue(0, 200)
        cf.ComputeScalarsOn()

        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(cf.GetOutputPort())

        a = vtk.vtkActor()
        a.SetMapper(m)

        r.AddActor(a)
        rw.Render()
        r.ResetCamera()

        # Render & capture z-buffer
        image = ch.vtkRenderToArray(rw, "Z")
        del rw

        # save as im
        pil.Image.fromarray(image).save("/tmp/test_exr.im")
        loaded_im = np.array(pil.Image.open("/tmp/test_exr.im"))

        # save as exr
        exr.save_depth(image, "/tmp/test_exr.exr")

        # load saved file and compare
        loaded_exr = exr.load_depth("/tmp/test_exr.exr")
        self.assertTrue(np.all(image == loaded_exr) and
                        np.all(loaded_exr == loaded_im))


if __name__ == '__main__':
    unittest.main()

#==============================================================================
# Copyright (c) 2015,  Kitware Inc., Los Alamos National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may
# be used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#==============================================================================
import unittest

from cinema_python import cinema_store
from cinema_python import explorers

class TestBasicFileStore(unittest.TestCase):
    def clean_up(self, cs, fname):
        import os
        for doc in cs.find():
            os.remove(cs._get_filename(doc.descriptor))
        os.remove(fname)

    def test_basic(self):
        thetas = [0,10,20,30,40]
        phis = [0,10,20]

        fname = "./test.json"
        cs = cinema_store.FileStore(fname)
        cs.filename_pattern = "data_{theta}_{phi}.txt"
        cs.add_parameter("theta", cinema_store.make_parameter('theta', thetas))
        cs.add_parameter("phi", cinema_store.make_parameter('phi', phis))

        s = set()

        for t in thetas:
            for p in phis:
                doc = cinema_store.Document({'theta':t,'phi':p})
                doc.data = str(doc.descriptor)
                s.add((t, p))
                cs.insert(doc)

        try:
            cs.save()

            s2 = set()

            cs2 = cinema_store.FileStore(fname)
            # Test load
            cs2.load()
            for doc in cs2.find():
                s2.add(tuple(doc.descriptor.values()))

            self.assertEqual(s, s2)

            # Test search
            docs = cs2.find({'theta' : 0})
            import ast
            for doc in docs:
                vals1 = [int(x) for x in doc.descriptor.values()]
                vals2 = ast.literal_eval(doc.data).values()
                self.assertEqual(vals1, vals2)
        except:
            self.clean_up(cs, fname)
            raise
        else:
            self.clean_up(cs, fname)

    def test_explorer(self):

        params = ["time","layer","slice_field","back_color"]

        cs = cinema_store.Store()
        cs.add_parameter("time", cinema_store.make_parameter("time", [0,1,2]))
        cs.add_parameter("layer", cinema_store.make_parameter("layer", ['outline','slice','background']))
        cs.add_parameter("slice_field", cinema_store.make_parameter("slice_field", ['solid_red', 'temperature', 'pressure']))
        cs.add_parameter("back_color", cinema_store.make_parameter("back_color", ['grey0', 'grey49']))

        class printDescriptor(explorers.Explorer):
            def __init__(self, *args):
                super(printDescriptor, self).__init__(*args)
                self.Descriptions = []

            def execute(self, desc):
                self.Descriptions.append(desc)

        # print "NO DEPENDENCIES"
        e = printDescriptor(cs, params, [])
        e.explore()
        self.assertEqual(len(e.Descriptions), 3*3*3*2)

        for desc in e.Descriptions:
            self.assertTrue('slice_field' in desc)
            self.assertTrue('back_color' in desc)

        # print "NO DEPENDENCIES AND FIXED TIME"
        e = printDescriptor(cs, params, [])
        e.explore({'time':3})

        self.assertEqual(len(e.Descriptions), 3*3*2)

        for desc in e.Descriptions:
            self.assertTrue('slice_field' in desc)
            self.assertTrue('back_color' in desc)

        # print "WITH DEPENDENCIES"
        cs.assign_parameter_dependence('slice_field', 'layer', ['slice'])
        cs.assign_parameter_dependence('back_color', 'layer', ['background'])

        ground_truth = { 'outline' : (False, False),
                         'slice' : (True, False),
                         'background' : (False, True)}

        e = printDescriptor(cs, params, [])
        e.explore()

        self.assertEqual(len(e.Descriptions), 6*3)

        # Check dependencies
        for desc in e.Descriptions:
            layer = desc['layer']
            match = ('slice_field' in desc, 'back_color' in desc)
            # print layer
            self.assertEqual(match, ground_truth[layer])

        # print "WITH DEPENDENCIES AND FIXED TIME"
        e = printDescriptor(cs, params, [])
        e.explore({'time':3})

        self.assertEqual(len(e.Descriptions), 6)

        # Check dependencies
        for desc in e.Descriptions:
            layer = desc['layer']
            match = ('slice_field' in desc, 'back_color' in desc)
            # print layer
            self.assertEqual(match, ground_truth[layer])

    def test_layers_fields(self):
        settings = []

        params = ["time", "layer", "component"]

        cs = cinema_store.Store()
        cs.add_parameter("time", cinema_store.make_parameter("time", ["0"]))
        cs.add_layer("layer", cinema_store.make_parameter("layer", ['outline','slice','background']))
        cs.add_field("component", cinema_store.make_parameter("component", ['z','RGB']), "layer", 'slice')

        def showme(self):
            settings[-1].append([self.name, True])
        def hideme(self):
            settings[-1].append([self.name, False])

        outline_control = explorers.LayerControl("outline", showme, hideme)
        slice_control = explorers.LayerControl("slice", showme, hideme)
        background_control = explorers.LayerControl("background", showme, hideme)

        field_control1 = explorers.LayerControl("z", showme, hideme)
        field_control2 = explorers.LayerControl("RGB", showme, hideme)

        layertrack = explorers.Layer("layer", [outline_control, slice_control, background_control])
        fieldtrack = explorers.Layer("component", [field_control1,field_control2])

        class printDescriptor(explorers.Explorer):
            def __init__(self, *args):
                super(printDescriptor, self).__init__(*args)

            def execute(self, desc):
                settings.append([desc])
                super(printDescriptor, self).execute(desc)

        e = printDescriptor(cs, params, [layertrack, fieldtrack])
        e.explore()
        ground_truth = \
        { 'outline' : { 'outline' : True, 'slice' : False, 'background' : False},
          'slice' : { 'outline' : False, 'slice' : True, 'background' : False},
          'background' : { 'outline' : False, 'slice' : False, 'background' : True}}
        slice_gt = [ {'z' : True, 'RGB' : False}, {'z' : False, 'RGB' : True}]
        for setting in settings:
            layer = setting[0]['layer']
            gt = ground_truth[layer]
            slice_settings = {}
            for s in setting[1:]:
                if s[0] not in ('z', 'RGB'):
                    self.assertEqual(s[1], gt[s[0]])
                else:
                    slice_settings[s[0]] = s[1]
            if slice_settings:
                self.assertTrue(slice_settings in slice_gt)

if __name__ == '__main__':
    unittest.main()



################## moved to tests/test.py #####################################
#def demonstrate_manual_populate(fname="/tmp/demonstrate_manual_populate/info.json"):
#    """Demonstrates how to setup a basic cinema store filling the data up with text"""
#
#    thetas = [0,10,20,30,40]
#    phis = [0,10,20]
#
#    cs = cinema_store.FileStore(fname)
#    cs.filename_pattern = "data_{theta}_{phi}.txt"
#    cs.add_parameter("theta", cinema_store.make_parameter('theta', thetas))
#    cs.add_parameter("phi", cinema_store.make_parameter('phi', phis))
#
#    for t in thetas:
#        for p in phis:
#            doc = cinema_store.Document({'theta':t,'phi':p})
#            doc.data = str(doc.descriptor)
#            cs.insert(doc)
#
#    cs.save()
#
#def demonstrate_populate(fname="/tmp/demonstrate_populate/info.json"):
#    """Demonstrates how to setup a basic cinema store filling the data up with text"""
#    #import explorers
#
#    cs = cinema_store.FileStore(fname)
#    cs.filename_pattern = "{theta}/{phi}.txt"
#    cs.add_parameter("theta", cinema_store.make_parameter('theta', [0,10,20,30,40]))
#    cs.add_parameter("phi", cinema_store.make_parameter('phi', [0,10,20,30,40]))
#
#    class Track(explorers.Track):
#        def execute(self, doc):
#            # we save the document's descriptor as the data in
#            # this dummy document.
#            doc.data = str(doc.descriptor)
#
#    e = explorers.Explorer(cs, ['theta', 'phi'], [Track()])
#    e.explore()
#
#def demonstrate_analyze(fname="/tmp/demonstrate_populate/info.json"):
#    """
#    this demonstrates traversing an existing cinema store and doing some analysis
#    (in this case just printing the contents) on each item
#    """
#    cs = cinema_store.FileStore(fname)
#    cs.load()
#    print cs.parameter_list
#    for doc in cs.find({'theta': 20}):
#        print doc.descriptor, doc.data
#
#def test_SFS(fname="/tmp/cinemaSFS/info.json"):
#    if not fname:
#        fname = "info.json"
#    fs = cinema_store.SingleFileStore(fname)
#    #fs.filename_pattern("{theta}_{phi}.txt")
#    fs.add_parameter('theta', {
#        "default": 60,
#        "type":  "range",
#        "values": [60, 90, 120, 150],
#        "label": "theta"
#        })
#    fs.add_parameter('phi', {
#        "default": 60,
#        "type":  "range",
#        "values": [60, 90, 120, 150],
#        "label": "phi"
#        })
#    #print fs._get_numslices()
#    print "INSERT DOC 60,150"
#    doc = cinema_store.Document({"phi": 60, "theta":150}, "Hello World")
